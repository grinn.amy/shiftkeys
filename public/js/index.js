import { LAYOUTS } from './layouts.js?v=2';

let allChars;
let rightShiftChars;
let leftShiftChars;

let score = 0;
let lives = 5;

let lastShift;
let status = 'pregame';
let clouds = new Set();
let cancelId;

window.addEventListener('keypress', (ev) => ev.preventDefault());

window.addEventListener('keydown', (ev) => {
  if (ev.code === 'ShiftRight') lastShift = 'right';
  if (ev.code === 'ShiftLeft') lastShift = 'left';
});

document.getElementById('options-form').addEventListener('submit', startGame);
document.getElementById('stop-btn').addEventListener('click', stopGame);

const app = document.getElementById('app');

function startGame(ev) {
  ev.preventDefault();
  const factor = ev.target.elements.factor.value;
  const layout = ev.target.elements.layout.value;

  allChars = LAYOUTS[layout].all;
  leftShiftChars = LAYOUTS[layout].right;
  rightShiftChars = LAYOUTS[layout].left;

  status = 'playing';
  app.setAttribute('data-status', 'playing');

  score = 0;
  lives = 5;
  updateScoreboard();

  let interval = 7;
  let duration = 18;

  const gameLoop = () => {
    clouds.add(createCloud(createText(), duration));
    cancelId = setTimeout(gameLoop, interval * 1000);

    interval *= factor;
    duration *= factor;
  };

  gameLoop();

  return false;
}

function stopGame() {
  if (status === 'playing') {
    status = 'pregame';
    app.setAttribute('data-status', 'pregame');
    for (let cloud of clouds) cloud.remove();
    clouds = new Set();
    clearTimeout(cancelId);
    window.alert(`Game over. Score: ${score}`);
  }
}

function createText() {
  const length = 3 + Math.floor(Math.random() * 5);

  let text = '';

  for (let i = 0; i < length; i++) {
    text += allChars[Math.floor(Math.random() * allChars.length)];
  }

  return text;
}

function updateScoreboard() {
  const livesEl = document.getElementById('lives');
  const scoreEl = document.getElementById('score');

  livesEl.innerHTML = `Lives: ${lives}`;
  scoreEl.innerHTML = `Score: ${score}`;
}

function completeCloud(cloud) {
  score++;
  clouds.delete(cloud);
  cloud.remove();
  updateScoreboard();
}

function failCloud(cloud) {
  cloud.remove();
  clouds.delete(cloud);
  lives--;
  updateScoreboard();
  if (lives === 0) stopGame();
}

function createCloud(text, duration) {
  const cloud = document.createElement('p');
  const span = document.createElement('span');
  span.appendChild(document.createTextNode(text));
  cloud.appendChild(span);
  app.appendChild(cloud);

  cloud.classList.add('cloud');

  cloud.style.left = `${30 + Math.random() * 40}vw`;
  cloud.style.setProperty('--duration', `${duration}s`);

  let matchedText = '';
  let remainingText = text;

  const listener = window.addEventListener('keyup', (ev) => {
    if (ev.key.length === 1) {
      if (letterMatchesKey(remainingText[0], ev.key)) {
        matchedText += remainingText[0];
        remainingText = remainingText.slice(1);
      } else {
        matchedText = '';
        remainingText = text;
      }

      if (remainingText.length === 0) {
        window.removeEventListener('keyup', listener);
        completeCloud(cloud);
      } else {
        while (cloud.firstChild) {
          cloud.removeChild(cloud.firstChild);
        }

        const matchedSpan = document.createElement('span');
        matchedSpan.classList.add('matched');
        matchedSpan.appendChild(document.createTextNode(matchedText));
        cloud.appendChild(matchedSpan);

        const remainingSpan = document.createElement('span');
        remainingSpan.appendChild(document.createTextNode(remainingText));
        cloud.appendChild(remainingSpan);
      }
    }
  });

  cloud.addEventListener('animationend', () => {
    window.removeEventListener('keyup', listener);
    failCloud(cloud);
  });

  return cloud;
}

function letterMatchesKey(letter, key) {
  if (letter !== key) return false;

  if (letter === letter.toLowerCase()) return true;

  if (leftShiftChars.includes(letter)) return lastShift === 'left';

  if (rightShiftChars.includes(letter)) return lastShift === 'right';

  return false;
}

export const LAYOUTS = {
  qwerty: {
    all: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`1234567890~!@#$%^&*(),./;\'[]\\<>?:"{}|',
    left: '~!@#$%QWERTASDFGZXCVB',
    right: '^&*()_+YUIOP{}|HJKL:"NM<>?',
  },
  bepo: {
    all: 'abcdefghijklmnopqrstuvwxyzéèàçABCDEFGHIJKLMNOPQRSTUVWXYZÉÈAÇ$"«»()@+-/*=%#1234567890°`!,;.:’?',
    left: '#12345BÉPOÈAUIE;ÀYX:K',
    right: '67890°`!VDLJZWÇCTSRNM?QGHF',
  },
  qwertz: {
    all: '!"§$%QWERTASDFG>YXCVB&/()=?HJKLÖÄ#NM;:_', // missing lowercase
    left: '!"§$%QWERTASDFG>YXCVB',
    right: '&/()=?HJKLÖÄ#NM;:_',
  },
  azerty: {
    // not implemented
    all: '',
    left: '',
    right: '',
  },
  qzerty: {
    // not implemented
    all: '',
    left: '',
    right: '',
  },
  dvorak: {
    all: '`1234567890[]\',.pyfgcrl/=\\aoeuidhtns-;qjkxbmwvz^&*(){}FGCRL?+|DHTNS_BMWVZ~!@#$%"<>PYAOEUI:QJKX',
    left: '~!@#$%"<>PYAOEUI:QJKX',
    right: '^&*(){}FGCRL?+|DHTNS_BMWVZ',
  },
  'programmer-dvorak': {
    all: '$&[{}(=*)+]!#;,.pyfgcrl/@\\aoeuidhtns-\'qjkxbmwvz902468`FGCRL?^|DHTNS_BMWVZ~%7531:<>PYAOEUI"QJKX',
    left: '~%7531:<>PYAOEUI"QJKX',
    right: '902468`FGCRL?^|DHTNS_BMWVZ',
  },
  colemak: {
    all: '`123456790-+qwfpgjluy;[]\\arstdhneio\'zxcvbkm,./~!@#$%QWFPGARSTDZXCVB^&*()_+JLUY:{}|HNEIO"KM<>?',
    left: '~!@#$%QWFPGARSTDZXCVB',
    right: '^&*()_+JLUY:{}|HNEIO"KM<>?',
  },
  workman: {
    all: '`1234567890-=qdrwbjfup;[]\\ashtgyneoi\'zxmcvkl,./~!@#$%QDRWBASHTGZXMCV^&*()_+JFUP:{}|YNEOI"KL<>?',
    left: '~!@#$%QDRWBASHTGZXMCV',
    right: '^&*()_+JFUP:{}|YNEOI"KL<>?',
  },
};
